
function everyone(got){

    const allNames = got.houses.reduce( (storeNames, currentVal) => {
        
        const familyNames = currentVal.people.reduce( ( storeFamName, currentFamName) => {
            storeFamName.push(currentFamName.name);
            return storeFamName;
        }, storeNames)

        return storeNames;
    }, []);

    return allNames;
}

module.exports = everyone;