
function countAllPeople(got){

    const totalPeople = got.houses.reduce( (storeCount, currentName) => {
        storeCount += currentName.people.length;
        return storeCount;
    }, 0);

    return totalPeople;
}

module.exports = countAllPeople;