
function surnameWithS(got){
    const nameList = got.houses.reduce( (storeName, currentObj) => {

        if(currentObj.name.includes('S')){
            currentObj.people.reduce( (storeSurname, currentName) => {
                storeName.push(currentName.name);
            }, storeName);
        }

        return storeName;
    }, []);

    return nameList;
}

module.exports = surnameWithS;