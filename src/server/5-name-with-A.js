
function nameWithA(got){

    const nameList = got.houses.reduce( (storeNames, currentObj) => {
        currentObj.people.reduce( (storeFamName, currentFamName) => {
            if(currentFamName.name.includes('a') || currentFamName.name.includes('A')){
                storeNames.push(currentFamName.name);
            }
        }, storeNames);

        return storeNames;
    }, []);

    return nameList;
}

module.exports = nameWithA;