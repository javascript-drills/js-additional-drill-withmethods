
function nameWithS(got){

    const namesList = got.houses.reduce( (storeName, currentObj) => {

        currentObj.people.reduce( (storeFamName, currentFamName) => {
            if(currentFamName.name.includes('s')|| currentFamName.name.includes('S')){
                storeName.push(currentFamName.name);
            }
        }, storeName);

        return storeName;
    }, []);

    return namesList;
}

module.exports = nameWithS;