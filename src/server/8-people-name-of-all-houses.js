
function peopleNameOfAllHouses(got){
    const solutionObject = got.houses.reduce((storeObject, currentObj) => {
        
        const nameList = currentObj.people.reduce( (solArray, currentKey) => {
            solArray.push(currentKey.name);
            return solArray;
        }, []);

        storeObject[currentObj['name']] = nameList;
        return storeObject;
    }, {});

    return solutionObject;
}

module.exports = peopleNameOfAllHouses;