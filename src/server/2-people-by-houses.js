
function peopleByHouses(got){

    const countByHouse = got.houses.reduce( (storeCount, current) => {
        if(storeCount[current.name] === undefined){
            storeCount[current.name] = current.people.length;
        }
        return storeCount;
    }, {});
    
    return countByHouse;
}

module.exports = peopleByHouses;