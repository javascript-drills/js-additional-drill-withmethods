
function surnameWithA(got){
    const nameList = got.houses.reduce( (storeName, currentObj) => {

        if(currentObj.name.includes('A')){
            currentObj.people.reduce( (storeSurname, currentName) => {
                storeName.push(currentName.name);
            }, storeName);
        }

        return storeName;
    }, []);

    return nameList;
}

module.exports = surnameWithA;